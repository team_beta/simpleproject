package beta.manager;

import beta.announcement.AlexandruAnnouncement;
import beta.announcement.Announcement;
import beta.announcement.LidiaAnnoucement;
import beta.announcement.IonutAnnouncement;
import beta.announcement.StefanAnnouncement;

import java.util.ArrayList;
import java.util.List;

public class AnnouncementBoard {

    private final static List<Announcement> announcements = new ArrayList<Announcement>();

    static {
        announcements.add(new StefanAnnouncement());
	    announcements.add(new AlexandruAnnouncement());
        announcements.add(new LidiaAnnoucement());
        announcements.add(new IonutAnnouncement());
    }

    public static void main(String[] args) {
        for (Announcement announcement : announcements) {
            System.out.println(announcement.saySomething());
        }
    }
}
